import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.Notifications 1.0
import Nemo.DBus 2.0

import "pages"
import "components"
import "cover"

ApplicationWindow
{
    id: bluepill

    property bool engineLoaded: false
    property int covstat
    property string myuser_id
    property string entertext

    // style stuff should be somewhere else

    property var usercolor: [ "#368bd6", "#ac3ba8", "#03b381", "#e64f7a", "#ff812d", "#2dc2c5", "#5c56f5", "#74d12c"]
    property string htmlcss: '<style>a:link { color: ' + Theme.highlightColor + '; }</style>'

    initialPage: Qt.resolvedUrl("pages/StartPage.qml")

    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    ConfigurationValue {
        id: favNotiValueConf
        key: "/apps/ControlPanel/bluepill/favNoti"
        defaultValue: false
    }

    ConfigurationValue {
        id: fontSizeConf
        key: "/apps/ControlPanel/bluepill/fontSize"
        defaultValue: 1.1
    }

    Timer {
        id: listenTimer
        interval: 10000
        onTriggered: {
            console.log("startListener triggered")
            clienthandler.restartTheListener()
        }
        running: false
        repeat: false
    }

    function hashCode(str) {
        var hash = 0
        var i
        var chr
        if (str.length === 0) {
            return hash
        }
        for (i = 0; i < str.length; i++) {
            chr = str.charCodeAt(i)
            hash = ((hash << 5) - hash) + chr
            hash |= 0
        }
        return Math.abs(hash);
    }

    function u_color(user_id) {
        return usercolor[hashCode(user_id) % 8]
    }

    DBusAdaptor {
        service: "harbour.bluepill.service"
        iface: "harbour.bluepill.service"
        path: "/harbour/bluepill/service"
        xml: "  <interface name=\"harbour.bluepill.service\">\n" +
             "    <method name=\"openPage\"/>\n" +
             "  </interface>\n"

        function openPage(roomid, roomname) {
            __silica_applicationwindow_instance.activate()
            pageStack.pop(null, true)
            pageStack.push(Qt.resolvedUrl("pages/RoomPage.qml"), { room_id: roomid, room_name: roomname } )
        }
    }

    Notification {
        id: logNotification
        category: "com.gitlab.cy8aer.bluepill"
        appIcon: "image://theme/icon-lock-chat"
        appName: "bluepill"
    }

    Notification {
        property string room_id
        property string room_name

        id: messageNotification
        category: "x-nemo.messaging.im"
        urgency: Notification.Critical
        // appIcon: "/usr/share/harbour-bluepill/images/q.png"
        appIcon: "image://theme/icon-lock-chat"
        appName: "bluepill"
        previewSummary: qsTr("New posts available")
        previewBody: qsTr("Click to view updates")
        body: qsTr("New Posts are available. Click to view.")

        maxContentLines: 5
        remoteActions: [ {
            name: "default",
            service: "harbour.bluepill.service",
            path: "/harbour/bluepill/service",
            iface: "harbour.bluepill.service",
            method: "openPage",
            arguments: [ room_id, room_name ]
        } ]
    }

    ClientHandlerPython {
        id: clienthandler
        onRestartListener: {
            console.log("Need to restart Listener in 10s")
            listenTimer.start()
        }
        onMRoomEvent: {
            if ( ! activeFocus && event.event.type === "m.room.message" && covstat === 0 && event.user_id !== myuser_id) {
                console.log("room Event", favNotiValueConf.value, event.rlevel)

                if (favNotiValueConf.value && event.rlevel !== "fav") {
                    console.log("No favorite!")
                    return
                }

                console.log("Notification:" + covstat)
                messageNotification.room_id = roomId
                messageNotification.room_name = event.rname
                messageNotification.summary = event.name + qsTr(" in ") + event.rname
                messageNotification.body = event.event.content.body
                messageNotification.previewBody = event.name + qsTr(" in ") + event.rname + ": " + event.event.content.body
                messageNotification.replacesId = new Date().getTime() / 1000
                messageNotification.publish()
            }
        }
        onJoinRoomFailed: {
            var e = JSON.parse(error)
            console.log("Room joining failed", e.error)
            logNotification.previewSummary = qsTr("Room login failed")
            logNotification.previewBody = e.error
            logNotification.publish()
        }
    }
}
