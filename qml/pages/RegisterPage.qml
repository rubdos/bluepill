import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaListView {
        anchors.fill: parent
        contentHeight: registerTitle.height + registerTitle.spacing + parameters.height + parameters.spacing

        Column {
            id: registerTitle

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                anchors.leftMargin: Theme.paddingMedium
                title: qsTr("Register")
            }
        }

        Column {
            id: parameters
            width: parent.width
            height: children.height
            anchors {
                top: registerTitle.bottom
                left: parent.left
                right: parent.right
                margins: Theme.paddingMedium
            }

            TextField {
                id: emailField

                width: parent.width
                height: Theme.itemSizeLarge
                placeholderText: label
                label: qsTr("Email address (optional)")
                inputMethodHints: Qt.ImhNoAutoUppercase
            }

            Button {
                id: registerButton
                text: qsTr("Register")
                onClicked: { }
            }
        }
    }
}
