<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="21"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="32"/>
        <source>[sticker]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="34"/>
        <source>[encrypted message]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="36"/>
        <source>[message redacted]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="38"/>
        <source>[unknown]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="111"/>
        <source>is typing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DashboardPage</name>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="24"/>
        <source>Create room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="20"/>
        <source>Enter room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="27"/>
        <source>StartChat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="33"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="105"/>
        <source>Favourites </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="139"/>
        <source>People </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="174"/>
        <source>Rooms </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="209"/>
        <source>Low priority </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="83"/>
        <source>Rooms</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventsListItem</name>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="14"/>
        <source>copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="20"/>
        <source>event source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="28"/>
        <source>cite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="36"/>
        <source>delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="115"/>
        <source> has sent an image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="179"/>
        <source>End to end encryption not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="181"/>
        <source>redacted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="184"/>
        <source>left the room</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="188"/>
        <source>is now </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="192"/>
        <source>entered the room</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JoinRoom</name>
    <message>
        <location filename="../qml/pages/JoinRoom.qml" line="29"/>
        <source>Join Room</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogStatusPage</name>
    <message>
        <location filename="../qml/pages/LogStatusPage.qml" line="41"/>
        <source>Starting engine...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="54"/>
        <location filename="../qml/pages/LoginPage.qml" line="141"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="76"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="93"/>
        <source>Client Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="100"/>
        <source>Custom Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="105"/>
        <source>Home server URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="115"/>
        <source>https://matrix.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="121"/>
        <source>Identity server URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="131"/>
        <source>https://vector.im</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="178"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="29"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="52"/>
        <source>Font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="57"/>
        <source>Markdown rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="58"/>
        <source>Render messages with Markdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="63"/>
        <source>Notify favourites only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="64"/>
        <source>Only notify for new messages from favourite rooms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="72"/>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="80"/>
        <source>Logging out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RedactMessage</name>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="15"/>
        <source>Redact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="21"/>
        <source>Reason for redaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="22"/>
        <source>Reason</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="28"/>
        <source>Do you really wish to redact (delete) this event? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegisterPage</name>
    <message>
        <location filename="../qml/pages/RegisterPage.qml" line="21"/>
        <location filename="../qml/pages/RegisterPage.qml" line="48"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RegisterPage.qml" line="42"/>
        <source>Email address (optional)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomListItem</name>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="15"/>
        <source>Favourite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="20"/>
        <source>Low priority</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="27"/>
        <source>Leave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="30"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RoomPage</name>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="49"/>
        <source>Preferences: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="53"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="121"/>
        <source>Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="122"/>
        <source>Collecting Posts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="21"/>
        <source>Connected...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="24"/>
        <source>Not connected, trying again later...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="55"/>
        <source>Starting engine...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextControl</name>
    <message>
        <location filename="../qml/components/TextControl.qml" line="57"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/TextControl.qml" line="111"/>
        <source> is typing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadSelector</name>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="17"/>
        <source>Upload...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="21"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="25"/>
        <source>Take a picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="98"/>
        <source>Send image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>harbour-bluepill</name>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="104"/>
        <source>New posts available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="105"/>
        <source>Click to view updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="106"/>
        <source>New Posts are available. Click to view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="137"/>
        <location filename="../qml/harbour-bluepill.qml" line="139"/>
        <source> in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="147"/>
        <source>Room login failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
