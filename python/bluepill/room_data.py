# Room Data

import sys

sys.path.append("/usr/share/harbour-bluepill/python")

from bluepill.singleton import Singleton
from bluepill.memory import Memory


class RoomData:
    """
    Persistant room data
    """

    def __init__(self):
        """
        Initialization
        """

        self.room_id = None
        self.name = None
        self.user_id = None
        self.display_name = None
        self.topic = None
        self.events = []
        self.users = []
        self.level = None
        self.lorder = 0
        self.direct = False
        self.end_token = None
        self.start_token = None
        self.avatar_url = None
        self.last_time = 0
        self.last_read = 0
        self.encrypted = False
        self.revent = None

    def save(self):
        """
        Save myself
        """

        Memory().save_object(self.room_id, self)

    def get_room_events(self):
        """
        yield room events
        """

        return self.events

    def set_revent(self, revent):
        """
        set my read room event position
        """

        self.revent = revent
        self.save()

    @property
    def unread_count(self):
        """
        get unread event count
        """

        try:
            revent = self.revent
        except:
            self.revent = revent = None

        pos = next(
            filter(
                lambda x: x[1]["event"]["event_id"] == revent,
                enumerate(self.events),
            ),
            (len(self.events) - 1, None),
        )[0]
        return len(self.events) - pos - 1


class RoomDataFactory(metaclass=Singleton):
    """
    Factory for fetching client data
    """

    def __init__(self):
        """
        Initialization
        """

        self._room_datas = {}

    def room_data(self, room_id):
        """
        Get RoomData object for room_id
        """

        if room_id in self._room_datas:
            return self._room_datas[room_id]

        room_data = Memory().get_object(room_id)
        if not room_data:
            room_data = RoomData()

        self._room_datas[room_id] = room_data
        return room_data
