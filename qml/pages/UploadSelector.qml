import QtQuick 2.5
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0

Page {
    id: uploadSelector
    allowedOrientations: Orientation.All
    property string previewfile
    property bool showpreview
    property string room_id

    SilicaListView {
        id: selectList
        anchors.fill: parent
        header: PageHeader {
            id: title
            title: qsTr("Upload...")
        }
        model: ListModel {
            ListElement {
                name: qsTr("Image")
                page: "file"
            }
            ListElement {
                name: qsTr("Take a picture")
                page: "CameraPage.qml"
            }
        }
        delegate: Item {
            width: ListView.view.width
            height: Theme.itemSizeSmall

            Label {
                text: name
                width: parent.width
                height: Theme.itemSizeMedium
                padding: Theme.paddingMedium
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (page === "file") {
                            previewfile = ""
                            pageStack.push(imagePickerPage)
                        } else {
                            pageStack.push(page)
                        }
                    }
                }
            }
        }
        footer: Column {
            spacing: Theme.paddingMedium
            width: parent.width
            id: douploadcolumn
            Connections {
                target: uploadSelector
                onShowpreviewChanged: {
                    douploadcolumn.visible = showpreview
                    uploadbutton.visible = showpreview
                    imageviewer.visible = showpreview && previewfile != ""
                }
                onPreviewfileChanged: imageviewer.source = previewfile
            }
            Connections {
                target: clienthandler
                onImageSent: {
                    console.log("Image sent!")
                    uploadbutton.uploading = false
                    pageStack.pop()
                }
                onImageSendFailed: {
                    uploadbutton.uploading = false
                }
            }

            Connections {
                target: camerapage
                onPhotoshot: {
                    console.log("Photo shot to " + imagefile)
                    showpreview = true
                    previewfile = imagefile
                }
            }

            Image {
                id: imageviewer
                source: ""
                width: parent.width
                fillMode: Image.PreserveAspectFit
                visible: false
                cache: false
                autoTransform: true
            }
            Button {
                id: uploadbutton
                property bool uploading: false
                visible: false
                text: qsTr("Send image")
                onClicked: {
                    uploading = true
                    clienthandler.sendImage(room_id, previewfile)
                }
                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: uploadbutton
                    running: uploadbutton.uploading
                }
            }
        }

        Component {
            id: imagePickerPage
            ImagePickerPage {
                onSelectedContentPropertiesChanged: {
                    var file = selectedContentProperties.filePath
                    previewfile = file
                    showpreview = true
                }
            }
        }
    }
}
